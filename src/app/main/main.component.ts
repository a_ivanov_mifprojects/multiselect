import {Component, forwardRef, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {List} from '../core/list-interface';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  list: List;
  form: FormGroup;

  test = ['Adventure', 'Action'];
  // test = ['Adventure'];
  // test = [10, 11];
  // test = [10];
  // test = '';

  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      multiselect: [this.test]
    });
  }

  ngOnInit() {
    this.setList();
  }

  setList() {
    this.route.data.subscribe(list => {
      this.list = list.list;
      console.log(this.list);
    });
  }

  submit(selected) {
    console.log(selected);
  }
}

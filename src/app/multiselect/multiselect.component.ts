import {Component, forwardRef, Input, OnInit, ViewChild} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {isArray} from 'util';
import {CdkOverlayOrigin, Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {CdkPortal} from '@angular/cdk/portal';

interface UnifiedList {
  bindLabel: string;
  bindValue: string | number;
}

@Component({
  selector: 'app-multiselect',
  templateUrl: './multiselect.component.html',
  styleUrls: ['./multiselect.component.scss'],
  providers: [{provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MultiselectComponent), multi: true}]
})
export class MultiselectComponent implements ControlValueAccessor, OnInit {
  @ViewChild(CdkOverlayOrigin) overlayOrigin: CdkOverlayOrigin;
  @ViewChild(CdkPortal) listElement;
  overlayList: OverlayRef;

  @Input() bindLabel: string;
  @Input() bindValue: string;
  @Input() multi: boolean | false;
  // tslint:disable-next-line:variable-name
  private _list: any;

  get list(): any[] {
    return this._list;
  }

  @Input()
  set list(value: any[]) {
    this.unifiedList = [];
    if (!this.setList(value)) {
      return;
    }
    this._list = value;
  }

  get value() {
    let bindOption: string;
    this.listIsObject ? bindOption = 'bindValue' : bindOption = 'bindLabel';

    return this.selected.map(item => {
      return item[bindOption];
    });
  }

  unifiedList: UnifiedList[];
  selected: UnifiedList[] = [];
  autocompleteList: UnifiedList[];
  disabled: boolean;
  listIsObject: boolean;

  constructor(private overlay: Overlay) {
  }

  ngOnInit() {
    this.crerateOverlayList();
  }

  crerateOverlayList() {
    this.overlayList = this.overlay.create(new OverlayConfig({
      positionStrategy: this.getPositionStrategy(), scrollStrategy: this.getScrollStrategy()
    }));
  }

  openList() {
    if (!this.overlayList.hasAttached()) {
      this.overlayList.attach(this.listElement);
    }
  }

  getPositionStrategy() {
    return this.overlay.position().flexibleConnectedTo(this.overlayOrigin.elementRef).withPositions([
      {originX: 'start', originY: 'top', overlayX: 'start', overlayY: 'bottom'},
      {originX: 'start', originY: 'bottom', overlayX: 'start', overlayY: 'top'}]);
  }

  getScrollStrategy() {
    return this.overlay.scrollStrategies.close();
  }

  updatePosition() {
    if (this.overlayList.hasAttached()) {
      this.overlayList.updatePosition();
    }
  }

  closeList(state?: boolean) {
    if (!state && this.overlayList.hasAttached()) {
      this.overlayList.detach();
    }
  }

  setList(list: any) {
    if (!(isArray(list))) {
      console.error('List have to be an array');
      return false;
    }

    if (list[0] instanceof Object) {
      this.listIsObject = true;
      if (typeof (this.bindValue) === 'undefined' || typeof (this.bindLabel) === 'undefined') {
        console.error('No \'bindLabel\' or \'bindValue\'');
        return false;
      }
    }

    this.unifyList(list);
    return true;
  }

  unifyList(list: UnifiedList[]) {
    this.unifiedList = list.map(item =>
      ({bindLabel: item[this.bindLabel] || item, bindValue: item[this.bindValue] || list.indexOf(item)}));
    this.autocompleteList = this.unifiedList;
  }

  setAutocompleteList(inputValue: string) {
    this.unifiedList = this.unifiedList.map(item => {
      if (String(item.bindLabel).toUpperCase().includes(inputValue.toUpperCase()) ||
        String(item).toUpperCase().includes(inputValue.toUpperCase())) {
        return item;
      }
    });
  }

  setValue(value: UnifiedList) {
    if (!this.disabled) {
      if (this.selected.includes(value)) {
        return;
      }

      this.multi ? this.selected.push(value) : this.selected[0] = value;
      this.writeValue(this.selected);
      this.updatePosition();
    }
  }

  removeItemFromSelected(item: UnifiedList) {
    this.selected.splice(this.selected.findIndex(i => i === item), 1);
    this.writeValue(this.selected);
    this.updatePosition();
  }

  onChange = (item: any) => {
  };

  onTouched = () => {
  };

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void) {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  writeValue(obj: any) {
    if (this.selected.length === 0 && obj !== null && obj.length !== 0) {
      !this.multi && obj.length > 1 ? console.error('Expected one argument, but receive more') : this.setSelectedFormInput(obj);
    }
    this.onChange(this.value);
  }

  setSelectedFormInput(element: UnifiedList[]) {
    let bindOption: string;
    this.listIsObject ? bindOption = 'bindValue' : bindOption = 'bindLabel';

    element.forEach(item => {
      const elementIndex = this.unifiedList.findIndex(i => i[bindOption] === item);
      this.selected.push(this.unifiedList[elementIndex]);
    });
  }

  cutString(item: string) {
    return `${item.substring(0, 7)}..`;
  }
}

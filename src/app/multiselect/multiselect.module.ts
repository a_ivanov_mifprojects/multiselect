import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MultiselectComponent} from './multiselect.component';
import {ClickOutsideDirective} from './click-outside.directive';
import {OverlayModule} from '@angular/cdk/overlay';
import {PortalModule} from '@angular/cdk/portal';

@NgModule({
  declarations: [
    MultiselectComponent,
    ClickOutsideDirective
  ],
  imports: [
    CommonModule,
    OverlayModule,
    PortalModule
  ],
  exports: [
    MultiselectComponent
  ]
})
export class MultiselectModule {
}

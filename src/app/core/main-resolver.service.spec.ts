import { TestBed } from '@angular/core/testing';

import { MainResolverService } from './main-resolver.service';

describe('MainResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MainResolverService = TestBed.get(MainResolverService);
    expect(service).toBeTruthy();
  });
});

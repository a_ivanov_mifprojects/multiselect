export interface List {
  list: ListItem[];
}

export interface ListItem {
  name: string;
  id: number;
  description: string;
}

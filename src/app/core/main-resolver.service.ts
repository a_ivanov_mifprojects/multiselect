import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ListService} from './list.service';

@Injectable({
  providedIn: 'root'
})
export class MainResolverService implements Resolve<any> {

  constructor(private listService: ListService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.listService.getList('data');
  }

}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {List} from './list-interface';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor(private http: HttpClient) {
  }

  getList(path: string) {
    return this.http.get<List>(`http://localhost:3000/${path}`);
  }
}
